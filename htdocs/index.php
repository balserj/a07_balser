<!DOCTYPE html>
<html lang="en">
<head>
<title>Assignment 07 -- Balser, Justin</title>
<link rel="stylesheet" type="text/css" href="styles.css">
<script src="a07_script.js"></script>

</head>
<body>
<form method="get" action="" name="mainForm" onsubmit="return(validateAccountInfo());">
   <fieldset>
      <legend>Account Information</legend>

      <p>
      <label for="firstname" id="firstnamelabel">First name</label><br/>
      <span id="errorFName"></span><br/>
      <input type="text" name="firstname" id="firstname"/>
      </p>

      <p>
      <label for="lastname" id="lastnamelabel">Last name</label><br/>
      <span id="errorLName"></span><br/>
      <input type="text" name="lastname" id="lastname"/>
      </p>

      <p>
        <label for="username" id="usernamelabel">Username</label><br/>
        <span id="errorUser"></span><br/>
        <input type="text" name="username" id="username"/>
      </p>

      <p>
        <label for="password" id="passwordlabel">Password</label><br/>
        <span id="errorPass"></span><br/>
        <input type="text" name="password" id="password"/>
      </p>

      <p>
        <label for="confirmpassword" id="confirmpasswordlabel">Confirm Password</label><br/>
        <span id="errorConfPass"></span><br/>
        <input type="text" name="confirmpassword" id="confirmpassword"/>
      </p>

      <p>
        <labelfor="email" id="emaillabel">Email Address</label><br/>
        <span id="errorEmail"></span><br/>
        <input type="text" name="email" id="email"/>
      </p>

      <p>
        <label for="confirmemail" id="confirmemaillabel">Confirm Email Address</label><br/>
        <span id="errorConfEmail"></span><br/>
        <input type="text" name="confirmemail" id="confirmemail"/>
      </p>

      <p>
      <label for="phone" id="phonelabel">Phone number [ex. (XXX) XXX-XXXX]</label><br/>
      <span id="errorPhone"></span><br/>
      <input type="text" name="phone" id="phone" maxwidth="14"/>
      </p>

      <p>
      <label for="address">Street Address</label><br/>
      <span id="errorAddress"></span><br/>
      <input type="text" name="address" id="address"/>
      </p>

      <p>
      <label for="city">City</label><br/>
      <span id="errorCity"></span><br/>
      <input type="text" name="city" id="city"/>
      </p>

      <p>
      <label for="state">State [Two letter abbreviation]</label><br/>
      <span id="errorState"></span><br/>
      <input type="text" name="state" id="state" maxlength="2"/>
      </p>

      <p>
      <label for="zip">Postal code [ex. XXXXX-XXX]</label><br/>
      <span id="errorZip"></span><br/>
      <input type="text" name="zip" id="zip"/>
      </p>

      <p>
        <label for="date">Date of birth [ex. XX-XX-XXXX]</label><br/>
        <span id="errorDOB"></span><br/>
        <input type="text" name="date" id="date"/>
      </p>

      <div class="rectangle centered">
         <input type="submit" value="Submit" class="btn"> <input type="reset" value="Clear Form" class="btn">
      </div>
   </fieldset>
</form>
</body>
</html>
