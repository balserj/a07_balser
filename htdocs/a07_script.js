function validateAccountInfo()
{
  var elementToCheck = document.getElementById('firstname');
  if (elementToCheck.value == "") {
      alert("You must input a name to be able to submit this form");
      document.getElementById('firstname').focus();
      return false;
  }
  else {
      var check = new RegExp(/^[a-z,.'-]+$/gm, 'i');
      document.getElementById("errorFName").innerHTML = "";
      if (check.test(elementToCheck.value)==false){
          alert("The first name input is not valid")
          document.getElementById('firstname').focus();
          document.getElementById('firstname').select();
          errorFName.style.color = "#ff6666";
          document.getElementById("errorFName").innerHTML = "Error: Provide a valid first name";
          return false;
      }
  }

  var elementToCheck = document.getElementById('lastname');
  if (elementToCheck.value == "") {
      alert("You must input a last name to be able to submit this form");
      document.getElementById('lastname').focus();
      return false;
  }
  else {
      var check = new RegExp(/^[a-z,.'-]+$/gm, 'i');
      document.getElementById("errorLName").innerHTML = "";
      if (check.test(elementToCheck.value)==false){
          alert("Invalid last name")
          document.getElementById('lastname').focus();
          document.getElementById('lastname').select();
          errorLName.style.color = "#ff6666";
          document.getElementById("errorLName").innerHTML = "Error -- Provide a valid last name";
          return false;
      }
  }

  var element = document.getElementById('username');
  if (element.value == "") {
      alert("You must input a username to be able to submit this form");
      document.getElementById('username').focus();
      return false;
  }
  else {
      var check = new RegExp(/^[a-z\d]{5,12}$/i, 'i');
      document.getElementById("errorUser").innerHTML = "";
      if (check.test(element.value)==false){
          alert("Invalid username")
          document.getElementById('username').focus();
          document.getElementById('username').select();
          errorUser.style.color = "#ff6666";
          document.getElementById("errorUser").innerHTML = "Error -- Provide a valid username";
          return false;
      }
  }

  var element = document.getElementById('password');
  if (element.value == "") {
      alert("You must input a password to be able to submit this form");
      document.getElementById('password').focus();
      return false;
  }
  else {
      var check = new RegExp(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{5,12}$/gm, 'i');
      document.getElementById("errorPass").innerHTML = "";
      if (check.test(element.value)==false){
          alert("Invalid password")
          document.getElementById('password').focus();
          document.getElementById('password').select();
          errorPass.style.color = "#ff6666";
          document.getElementById("errorPass").innerHTML = "Error -- Provide a valid password";
          return false;
      }
  }

  var element = document.getElementById('confirmpassword');
  var element1 = document.getElementById('password');
  if (element.value == "") {
      alert("You must input the same password again to be able to submit this form");
      document.getElementById('confirmpassword').focus();
      return false;
  }
  else {
      var check = new RegExp(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{5,12}$/gm, 'i');
      document.getElementById("errorConfPass").innerHTML = "";
      if ((check.test(element.value)==false) && element != element1) {
          alert("Not the same password")
          document.getElementById('confirmpassword').focus();
          document.getElementById('confirmpassword').select();
          errorConfPass.style.color = "#ff6666";
          document.getElementById("errorConfPass").innerHTML = "Error -- Provide the same password";
          return false;
      }
  }

  var element = document.getElementById('email');
  if (element.value == "") {
      alert("You must input a email address to be able to submit this form");
      document.getElementById('email').focus();
      return false;
  }
  else {
      var check = new RegExp(/^([a-z\d\.-]+)@([a-z\d-]+)\.([a-z]{2,8})(\.[a-z]{2,8})?$/gm, 'i');
      document.getElementById("errorEmail").innerHTML = "";
      if (check.test(element.value)==false){
          alert("Invalid email address entered")
          document.getElementById('email').focus();
          document.getElementById('email').select();
          errorEmail.style.color = "#ff6666";
          document.getElementById("errorEmail").innerHTML = "Error -- Provide a valid email address";
          return false;
      }
  }

  var element = document.getElementById('confirmemail');
  var element1 = document.getElementById('email');
  if (element.value == "") {
      alert("You must input the same email address to be able to submit this form");
      document.getElementById('confirmemail').focus();
      return false;
  }
  else {
      var check = new RegExp(/^([a-z\d\.-]+)@([a-z\d-]+)\.([a-z]{2,8})(\.[a-z]{2,8})?$/gm, 'i');
      document.getElementById("errorConfEmail").innerHTML = "";
      if ((check.test(element.value)==false) && element != element1) {
          alert("Not the same email address as entered before")
          document.getElementById('confirmemail').focus();
          document.getElementById('confirmemail').select();
          errorConfEmail.style.color = "#ff6666";
          document.getElementById("errorConfEmail").innerHTML = "Error -- Provide the same email address";
          return false;
      }
  }

  var element = document.getElementById('phone');
  if (element.value == "") {
      alert("You must input a phone number to be able to submit this form");
      document.getElementById('phone').focus();
      return false;
  }
  else {
      var check = new RegExp(/^(\([0-9]{3}\)\s*|[0-9]{3}\-)[0-9]{3}-[0-9]{4}$/gm, 'i');
      var check2 = new RegExp(/^\d{10}$/gm, 'i');
      document.getElementById("errorPhone").innerHTML = "";
      if ((check.test(element.value)==false) && (check2.test(element.value)==false))
      {
          alert("Invalid phone number entered")
          document.getElementById('phone').focus();
          document.getElementById('phone').select();
          errorPhone.style.color = "#ff6666";
          document.getElementById("errorPhone").innerHTML = "Error -- Provide a valid phone number";
          return false;
      }
  }

  var element = document.getElementById('address');
  if (element.value == "") {
      alert("You must input a street address to be able to submit this form");
      document.getElementById('address').focus();
      return false;
  }
  else {
      var check = new RegExp(/^\d{1,6}\040([A-Z]{1}[a-z]{1,}\040[A-Z]{1}[a-z]{1,})$|([A-Z]{1}[a-z]{1,}\040[A-Z]{1}[a-z]{1,}\040[A-Z]{1}[a-z]{1,})$/gm, 'i');
      document.getElementById("errorAddress").innerHTML = "";
      if (check.test(element.value)==false){
          alert("Invalid street address entered")
          document.getElementById('address').focus();
          document.getElementById('address').select();
          errorAddress.style.color = "#ff6666";
          document.getElementById("errorAddress").innerHTML = "Error -- Provide a valid street address";
          return false;
      }
  }

  var element = document.getElementById('city');
  if (element.value == "") {
      alert("You must input a city to be able to submit this form");
      document.getElementById('city').focus();
      return false;
  }
  else {
      var check = new RegExp(/^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/gm, 'i');
      document.getElementById("errorCity").innerHTML = "";
      if (check.test(element.value)==false){
          alert("Invalid city entered")
          document.getElementById('city').focus();
          document.getElementById('city').select();
          errorCity.style.color = "#ff6666";
          document.getElementById("errorCity").innerHTML = "Error -- Provide a valid city";
          return false;
      }
  }

  var element = document.getElementById('state');
  if (element.value == "") {
      alert("You must input a state to be able to submit this form");
      document.getElementById('state').focus();
      return false;
  }
  else {
      var check = new RegExp(/.*$/gm, 'i');
      document.getElementById("errorState").innerHTML = "";
      if (check.test(element.value)==false){
          alert("Invalid state abbreviation entered")
          document.getElementById('state').focus();
          document.getElementById('state').select();
          errorState.style.color = "#ff6666";
          document.getElementById("errorState").innerHTML = "Error -- Provide a valid state";
          return false;
      }
  }

  var element = document.getElementById('zip');
  if (element.value == "") {
      alert("You must enter a zip code to be able to submit this form");
      document.getElementById('zip').focus();
      return false;
  }
  else {
      var check = new RegExp(/(^\d{5}$)|(^\d{5}-\d{3}$)/gm, 'i');
      document.getElementById("errorZip").innerHTML = "";
      if (check.test(element.value)==false){
          alert("Invalid zip code entered")
          document.getElementById('zip').focus();
          document.getElementById('zip').select();
          errorZip.style.color = "#ff6666";
          document.getElementById("errorZip").innerHTML = "Error -- Provide a valid zip code";
          return false;
      }
  }

  var element = document.getElementById('date');
  if (element.value == "") {
      alert("You must input a date of birth to be able to submit this form");
      document.getElementById('date').focus();
      return false;
  }
  else {
      var check = new RegExp(/^([0-9]{2})-([0-9]{2})-([0-9]{4})$/gm, 'i');
      document.getElementById("errorDOB").innerHTML = "";
      if (check.test(element.value)==false){
          alert("Invalid date of birth entered")
          document.getElementById('date').focus();
          document.getElementById('date').select();
          errorDOB.style.color = "#ff6666";
          document.getElementById("errorDOB").innerHTML = "Error -- Provide a valid date of birth";
          return false;
      }
  }
  return true;
}
